package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {
    @Test
    public void testlogin() {
        //step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectpage());

        //step 2 click on the login link and the login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));

    }
}
